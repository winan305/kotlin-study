
inline fun highOrderFunction(param: Int, lambda: (Int) -> Unit) {
    println("Something.. $param")
    lambda.invoke(param)
}

inline fun highOrderFunction2(param: Int, lambda1: (Int) -> Unit, noinline lambda2: (Int) -> Unit) {
    // Something
}

fun main() {
    val helloWorld: () -> Unit = { println("Hello World") }
    val plus: (Int, Int) -> (Int) = { a: Int, b: Int -> a+b }
    val plusPrint: (Int, Int) -> Unit = { a: Int, b: Int -> println("Plus pinrt :: ${a+b}") }

    helloWorld.invoke()
    println("Plus :: ${plus(1, 2)}")
    plusPrint.invoke(1, 2)

//    highOrderFunction(1, {
//        println("Handle Lambda Result :: $it")
//    })

    highOrderFunction(1) {
        println("Handle Lambda Result :: $it")
    }
}