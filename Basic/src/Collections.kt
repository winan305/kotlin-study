fun main() {
    val list: List<String> = listOf(
            "str1", "str2", "str3"
    )

    val mutableList: MutableList<String> = mutableListOf(
            "str1", "str2", "str3"
    )

    val set: Set<String> = setOf(
            "str1", "str2", "str3"
    )

    val mutableSet: MutableSet<String> = mutableSetOf(
            "str1", "str2", "str3"
    )

    val map: Map<Int, String> = mapOf(
            1 to "str1",
            2 to "str2",
            3 to "str3"
    )

    val mutableMap: MutableMap<Int, String> = mutableMapOf(
            1 to "str1",
            2 to "str2",
            3 to "str3"
    )

    // add가 존재하지 않는다.
//    list.add("str4")
//    set.add("str4")

    // add가 존재한다.
    mutableList.add("str4")
    mutableSet.add("str4")

    // put이 존재하지 않는다. map.put(4, "str4")
//    map[4] = "str4"

    // mutableMap.put(4, "str4")
    mutableMap[4] = "str4"
}
