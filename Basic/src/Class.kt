open class ExampleClass {
    var a: Int = 0
    var b: Int = 0
    var c: Int = 0

//    constructor()
    constructor(a: Int)
    constructor(a: Int, b: Int)
    constructor(a: Int, b: Int, c: Int)

    // constructor(a: Int = 0, b: Int = 0, c: Int = 0)
}

//class ExampleClass(val a: Int, val b: Int, val c: Int) {
//    fun printValues() {
//        println("a = $a, b = $b, c = $c")
//    }
//}

class ExampleClass2 : ExampleClass {
    var d: Int = 0

    constructor() {
        this.d = 0
    }

    constructor(d: Int): super(2, 3) {
        this.d = 2
    }
}

//
abstract class Animal constructor(val name: String) {
    abstract fun eat(food: String)
    abstract fun sleep()

    override fun toString(): String {
        return "이 동물의 이름은 $name 이다."
    }
}
//
interface IDog {
    fun bark(sound: String)
    fun run(speed: Float)
    fun bite(power: Int)
}
//
class Dog(name: String, private val type: String) : Animal(name), IDog {
    override fun eat(food: String) {
        println("강아지 ${name}은(는) ${food}를 먹었다.")
    }

    override fun sleep() {
        println("강아지 ${name}은(는) 잠을 잔다.")
    }

    override fun toString(): String {
        return "이 강아지의 이름은 $name 이고 종은 $type 이다."
    }

    override fun bark(sound: String) {
        println("강아지 ${name}은(는) ${sound}소리를 내며 짖었다.")
    }

    override fun run(speed: Float) {
        println("강아지 ${name}은(는) ${speed}속도로 달렸다.")
    }

    override fun bite(power: Int) {
        println("강아지 ${name}은(는) ${power}의 힘으로 물었다.")
    }
}

fun main() {

//    val aDog = Dog("Super Dog", "retriever")
//
//
//    aDog.run {
//        println(this)
//        eat("최고급 사료")
//        bark("으르렁 컹컹")
//        run(12.5f)
//        bite(3)
//        sleep()
//    }
}