fun main() {
    // 변수 선언
    var intVar: Int = 3
    val intVal: Int = 3

    intVar = 4 // var은 변경 가능
//    intVal = 4 // val은 변경 불가능, 컴파일 에러가 발생한다.

    //  val stringVar1: String = "string"
    val stringVar1 = "string" // 타입 추론으로 인해 변수명 뒤의 타입 선언 생략 가능

    // 문자열 안에 $ 기호로 변수를 사용할 수 있음(String Interpolation)
    val stringVal2 = "intVar : $intVar, intVal : $intVal"
    // {}를 사용하여 식도 가능함
    val stringVal3 = "intVar + intVal = ${intVar + intVal}"

    // if ~ else if ~ else
    if(intVar > 3) {
        print("intVar > 3")
    } else if(intVar < 2) {
        print("intVar < 2")
    } else {
        print("What is intVar?")
    }

    // in을 사용해 어떤 범위안에 있는지도 체크 가능
    if(intVal in 0..3) {
        // Do something
    }

    // when. switch를 생각해보자.
    when(intVar) {
        0 -> print("int Var is 0")
        1, 2 -> print("int Var is 1 or 2")
        3 -> print("int Var is 3")
        else -> print("What is intVar?")
    }

    // for loop
    for(i in 0 until 10) {
        print("i :: $i")
    }

    // Colletion을 활용할 수도 있음
    val strs = listOf("a", "b", "c", "d")
    for(str in strs) {

    }

    // while loop
    var index = 0
    while(index < 10) {
        print("Index :: $index")
        index++
    }
}